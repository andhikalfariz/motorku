MotorKu!
I.	LatarBelakang 
 	Perawatan kendaraan bermotor adalah suatu hal yang penting. Hal ini saya baru sadari kembali ketika kakek saya (tentu saja sudah berumur) lupa untuk mengganti oli motornya dalam jangka waktu yang cukup lama sehingga mesin motor beliau rusak sampai tidak bisa digunakan. Akhirnya beliau terpaksa mengeluarkan uang sebesar 1 juta untuk mereparasi motornya karena kelupaan tersebut. Padahal, sehararusnya biasa service dan ganti oli hanya butuh uang 100 ribu untuk seminimal-minimalnya 6 bulan sekali. Oleh karena itu, MotorKu hadir sebagai solusi untuk memecahkan masalah sepermotoran duniawi ini. “MotorKu! Sahabat Motor Mu yang Lupa Diservis!”

II.	Ide 
 	Aplikasi ini akan berbentuk web application yang akan diimplementasikan dengan framework Django. Fitur-fitur yang akan diimplementasikan dalam aplikasi ini antara lain :
a)	User Authentication
 	Fitur ini nanti akan menyimpan data-data user berupa profil, list kendaraan yang dimiliki, serta riwayat dan progress perawatan kendaraan-kendaraan yang dia miliki. Nantinya tiap user juga akan memiliki level. Level tersebut bisa naik dengan mengumpulkan exp. Tiap aktivitas seperti servis dan cuci motor akan memiliki poin 
b)	Servis!
 	Fitur ini akan mengupdate riwayat dan progres perawatan kendaraan anda seperti ganti oli, ganti ban, tambah angin, isi bensin, bersih-bersih mesin, dan lain-lain.
c)	Cuci Motor!
 	Fitur ini akan mengupdate riyawat dan progres pencucian kendaraan Anda. Kemungkinan akan dibedakan antara cuci sendiri dan cuci di tempat cuci motor (yang menggunakan steam, hidrolik, atau snow wash).
d)	Reminder
 	Fitur ini akan mengingatkan Anda jika kendaraan Anda belum diservis atau dicuci dalam jangka waktu tertentu. Tiap aspek akan memiliki jangka waktu yang berbeda. Misalnya ganti oli akan diingatkan ketika sudah 6 bulan, cuci motor 1 bulan, dst.

e)	Cari Bengkel
 	Fitur ini akan menampilkan bengkel terdekat dari tempat Anda berada. Akan diimplementasikan dengan GPS dan GoogleMaps API.
f)	Cari Cuci
 	Fitur ini akan menampilkan tempat cuci motor terdekat dari tempat Anda berada. Akan diimplementasikan dengan GPS dan GoogleMaps API.



